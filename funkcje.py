import cv2
import numpy as np
import imutils
from imutils import perspective


def getContours(img, cThr=[50,100], showCanny=False, 
                minArea=30000, filter=0, draw =False):
    imgGray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    imgBlur = cv2.GaussianBlur(imgGray, (5,5), 0)
    imgCanny = cv2.Canny(imgBlur, cThr[0], cThr[1])
    imgDial = cv2.dilate(imgCanny,np.ones((5,5)),iterations=1)
    imgErode = cv2.erode(imgDial,np.ones((5,5)),iterations=1)
    
    if showCanny: 
        imgErode1 = cv2.resize(imgErode, (0,0), None, 0.2,0.2)
        cv2.imshow('Canny', imgErode1)

    contours, hiearchy = cv2.findContours(imgErode, 
                                          cv2.RETR_EXTERNAL, 
                                          cv2.CHAIN_APPROX_SIMPLE)
    finalCountours = []
    for i in contours:
        area = cv2.contourArea(i)
        if area > minArea:
            peri = cv2.arcLength(i, True)
            approx = cv2.approxPolyDP(i, 0.02*peri,True)
            bbox = cv2.boundingRect(approx)
            if filter > 0:
                if len(approx) == filter:
                    finalCountours.append([len(approx), 
                                           area, approx,bbox,i])

            else:
                finalCountours.append([len(approx), area, 
                                       approx,bbox,i])

    finalCountours = sorted(finalCountours,
                            key = lambda dist_left:dist_left[1] ,reverse=True )

    if draw:
        for con in finalCountours:
            cv2.drawContours(img, con[4], -1, (0,0,255), 2)
            

    return img, finalCountours, contours

def reorder(Points):
    Points = Points.reshape((4,2))
    PointsNew = np.zeros_like(Points)

    add = Points.sum(1)
    diff = np.diff(Points,axis=1)
    PointsNew[0] = Points[np.argmin(add)]
    PointsNew[1] = Points[np.argmin(diff)]
    PointsNew[2] = Points[np.argmax(diff)]
    PointsNew[3] = Points[np.argmax(add)]


    return PointsNew


def cutImg(img, points, w, h,pad=2):
    points = reorder(points)

    pts1 = np.float32(points)
    pts2 = np.float32([[0,0],[w,0], [0,h], [w,h]])
    matrix = cv2.getPerspectiveTransform(pts1,pts2)
    imgCut = cv2.warpPerspective(img, matrix, (w,h))
    imgCut = imgCut[pad:imgCut.shape[0]-pad,pad:imgCut.shape[1]-pad]

    return imgCut

def distance(ptA,ptB):
    return ((ptB[0]-ptA[0])**2 + (ptB[1]-ptA[1])**2)**0.5, ptA, ptB

def midpoint(ptA, ptB):
    return ((ptA[0] + ptB[0]) * 0.5, (ptA[1] + ptB[1]) * 0.5)

def contourPoint(img, cont):

    for c in cont:
        cv2.polylines(img,[c], True,(0,255,0), 2)

        rect = cv2.minAreaRect(c)
        rect = cv2.BoxPoints(rect) if imutils.is_cv2() else cv2.boxPoints(rect)
        rect = np.array(rect, dtype="int")
        rect = perspective.order_points(rect)
        #cv2.drawContours(img, [rect.astype("int")], -1, (0, 255, 0), 2)
        #for (dist_left, y) in rect:
        #    cv2.circle(img, (int(dist_left), int(y)), 5, (255, 0, 0), -1)

        (bl, tl, tr, br) = rect
        mu = midpoint(tl, tr)
        mb = midpoint(bl, br)

        ml = midpoint(tl, bl)
        mr = midpoint(tr, br)

        #cv2.circle(img, (int(ml[0]), int(ml[1])), 5, (0, 0, 255), -1) 
        #cv2.circle(img, (int(mr[0]), int(mr[1])), 5, (255, 0, 0), -1) 
        cv2.circle(img, (int(mu[0]), int(mu[1])), 5, (255, 0, 0), -1) 
        cv2.circle(img, (int(mb[0]), int(mb[1])), 5, (255, 0, 0), -1) 

    return img, mu, mb, ml, mr

def getDimensions(final_image, cont, ml, mr, mu, mb, r_min = 1000.0, l_min = 1000.0, left_min = [], right_min = [], scale = 2 ):

    for c in cont[0][4]:
               
        l_dist1 = np.array(distance([ml[0], ml[1]],c[0]), dtype=object)
        l_dist1_round = round(l_dist1[0],2)                
        r_dist1 = np.array(distance([mr[0], mr[1]],c[0]), dtype=object)
        r_dist1_round = round(r_dist1[0],2)               
        if l_dist1_round < l_min:
            l_min = l_dist1_round
            left_min.append(l_min)  
        
        if r_dist1_round < r_min:
            r_min = r_dist1_round
            right_min.append(r_min) 
   
    for c in cont[0][4]:
        dist_left = np.array(distance([ml[0], ml[1]],c[0]), dtype=object)
        dist_left_round = round(dist_left[0],2)
        if dist_left_round < left_min[-2]: 
            break
    for c in cont[0][4]:
        dist_right = np.array(distance([mr[0], mr[1]],c[0]), dtype=object)
        dist_right_round = round(dist_right[0],2)
        if dist_right_round < right_min[-2]: 
            break
    
    diam_dist = distance((int(dist_right[2][0]), 
                          int(dist_right[2][1])),
                         (int(dist_left[2][0]), int(dist_left[2][1])))
    diam_dist = round(diam_dist[0],1)/(float(scale)*10)   
                        
    length_dist = distance([mu[0], mu[1]],[mb[0], mb[1]])
    length_dist = round(length_dist[0],1)/(float(scale)*10)

    length, diam = selection(length_dist,diam_dist)

    cv2.circle(final_image, (int(dist_left[2][0]), int(dist_left[2][1])), 
               5, (255, 0, 0), -1)
    cv2.circle(final_image, (int(dist_right[2][0]), int(dist_right[2][1])), 
               5, (255, 0, 0), -1)
    cv2.line(final_image, (int(dist_left[2][0]), int(dist_left[2][1])),  
             (int(dist_right[2][0]), int(dist_right[2][1])), (255, 0, 255), 2)
    cv2.line(final_image, (int(mu[0]), int(mu[1])), 
             (int(mb[0]), int(mb[1])), (255, 0, 255), 2)

    cv2.putText(final_image, "{:.1f}cm".format(diam_dist), 
                (int(dist_left[2][0] - 15), int(dist_left[2][1] - 10)), 
                cv2.FONT_HERSHEY_SIMPLEX, 0.65, (0, 0, 255), 2)
    cv2.putText(final_image, "{:.1f}cm".format(length_dist), 
                (int(dist_right[2][0] - 30), (int(dist_right[2][1]) + 20)), 
                cv2.FONT_HERSHEY_SIMPLEX, 0.65, (0, 0, 255), 2)
    cv2.putText(final_image, length, (80, 80), cv2.FONT_HERSHEY_SIMPLEX, 0.5, 
                (0, 0, 255), 2)
    cv2.putText(final_image, diam, (80, 100), cv2.FONT_HERSHEY_SIMPLEX, 0.5, 
                (0, 0, 255), 2)


    return final_image 


def selection(length_dist, diam_dist):
    diam = "Srednica: w normie"
    length = ""
    if diam_dist > 3.5 and diam_dist < 5.0:

        if length_dist > 30.0 and length_dist < 40.0:
            length = "Klasa: Duze"
        elif length_dist > 20.0 and length_dist < 30.0:
            length = "Klasa: Srednie"
        elif length_dist > 10.0 and length_dist < 20.0:
            length = "Klasa: Male"
        else:
            length = "Dlugosc poza zakresem"
    else:
        diam = "Srednica: poza zakresem"
    return length, diam




