from picamera.array import PiRGBArray #--
from picamera import PiCamera #--
import time #--
import cv2
import numpy as np
import funkcje
import imutils
from imutils import perspective

#path = 'image_path'
scale = 2
wP = 204*scale
hP = 297*scale

camera = PiCamera() #--
camera.resolution = (640, 480) #--
camera.framerate = 32 #--
rawCapture = PiRGBArray(camera, size=(640, 480)) #--
time.sleep(0.1) #--


#while True:                    # w przypadku uzycia algorytmu bez PiCamery trzeba odkomentowac aktualnie zakomentowane linie kodu
#    img=cv2.imread(path)       # oraz zakomantowac linie oznaczone znakiem  #-

for frame in camera.capture_continuous(rawCapture, format="bgr", use_video_port=True): #--
    img = frame.array #--
    imgCont, finalConts, allContours = funkcje.getContours(img, minArea=10000, filter=4, cThr=[50,50])      

    if len(finalConts)!=0:

        biggest = finalConts[0][2]
        imgCut = funkcje.cutImg(img, biggest, wP, hP, pad=4) 
        imgCont2, finalConts2, allConts2 = funkcje.getContours(imgCut, minArea=10000, cThr=[82,35], draw = True)  

        if len(finalConts2) != 0:

            image, mu, mb, ml, mr = funkcje.contourPoint(imgCut,allConts2)

            final_image = funkcje.getDimensions(image, finalConts2, ml, mr, mu, mb)

        cv2.imshow("Final Image", final_image)
          
    key = cv2.waitKey(1) & 0xFF
    rawCapture.truncate(0) #--
    if key == ord("q"):
        break

